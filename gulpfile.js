var gulp = require('gulp'),
  connect = require('gulp-connect'),
  inject = require('gulp-inject'),
  wiredep = require('wiredep').stream;
var mainBowerFiles = require('main-bower-files');
var gulpBowerFiles = require('gulp-bower-files');
var clean = require('gulp-clean');
var angularFilesort = require('gulp-angular-filesort');

var postcss = require('gulp-postcss');
var simplevars = require('postcss-simple-vars');
var autoprefixer = require('autoprefixer-core');
var nestedcss = require('postcss-nested');
var corepostcss = require('postcss');

gulp.task('clean', function () {
    return gulp.src('dev', {read: false})
        .pipe(clean());
});

gulp.task('components', function() {
    return gulp.src(mainBowerFiles(), { base: 'app/bower_components' })
        .pipe(gulp.dest('./dev/bower_components'))
});

var paths = {
  js: ['app/js/**/*.js'],
  images: 'app/img/**/*',
  styles: 'app/css/*.css',
  html: ['app/*.html', 'app/views/**/*.html']
};

gulp.task('inject', function() {
  gulp.src('./app/index.html')
  .pipe(inject(gulp.src(mainBowerFiles(), {read: false}), {name: 'bower', ignorePath: 'app/'}))
  .pipe(inject(
    gulp.src('./app/css/*.css'), {ignorePath: 'app/'}
  ))
  .pipe(inject(
    gulp.src(paths.js) // gulp-angular-filesort depends on file contents, so don't use {read: false} here
      .pipe(angularFilesort()) , {ignorePath: 'app/'}
    ))
  .pipe(gulp.dest('./dev'));

});
gulp.task('dev-connect', function() {
  connect.server({
    root: 'dev',
    port:8000,
    host: '0.0.0.0',
    fallback: 'dev/index.html'
  });
});

gulp.task('style', function () {
  gulp.src('./app/css/**')
    .pipe(gulp.dest('./dev/css'));
});

gulp.task('css', function () {
	var processors = [
		autoprefixer({browsers: ['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4']}),
		simplevars,
		nestedcss,
	];
	return gulp.src('./app/css/**')
		.pipe(postcss(processors))
		// .pipe(postcss(processors).on('error', gutil.log))
		.pipe(gulp.dest('./dev/css'));
});

gulp.task('js', function () {
  gulp.src(paths.js, {'base': 'app'})
    .pipe(gulp.dest('./dev'));
});

gulp.task('views', function () {
  gulp.src(paths.html, {'base': 'app'})
    .pipe(gulp.dest('./dev'));
});

gulp.task('watch', function() {
  gulp.watch(paths.js, ['js']);
  gulp.watch(paths.styles, ['css']);
  gulp.watch(paths.html, ['inject', 'views']);
});

gulp.task('dev', ['css', 'js', 'views', 'components', 'inject', 'watch']);

gulp.task('default', ['dev', 'dev-connect']);
// gulp.task('build', ['clean', 'components', 'inject']);
