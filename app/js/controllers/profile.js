'use strict';

angular.module('wishList')
.controller('ProfileController', ['$scope', '$rootScope', '$localStorage',
'$routeParams', '$location', 'AuthService',
function($scope, $rootScope, $localStorage, $routeParams, $location, AuthService) {
  $scope.formData = $localStorage.user;
  $scope.edit = function() {

      AuthService.editUser($scope.formData, function(res) {
          if (res.type == false) {
              alert(res.data)
          } else {
            AuthService.setupUser(res)
              window.location = "/"
          }
      }, function() {
          $rootScope.error = 'Failed to signup';
      })
  };
}]);
