'use strict';

angular.module('wishList')
.controller('ListController', ['$scope', '$routeParams', '$location', 'WishlistServer', function($scope, $routeParams, $location, WishlistServer) {
  $scope.greeting = 'Hola!';

  // $scope.items = [
  //   {
  //     img: 'http://www.peta.org/wp-content/uploads/2016/02/Goats-maximili-602x403.jpg',
  //     name: 'Thing',
  //     description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
  //   },
  //   {
  //     img: 'http://www.peta.org/wp-content/uploads/2016/02/Goats-maximili-602x403.jpg',
  //     name: 'Thing',
  //     description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
  //   },
  //   {
  //     img: 'http://www.peta.org/wp-content/uploads/2016/02/Goats-maximili-602x403.jpg',
  //     name: 'Thing',
  //     description: 'Lorem ipsum aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
  //   },
  // ]

  $scope.formData = {};

  $scope.setFormData = function (item) {
    $scope.formData = item;
  }
  // GET =====================================================================
  if (!$routeParams.list_id) {
    WishlistServer.getLists()
      .success(function(data) {
        $scope.items = data;
      });
  } else {
    WishlistServer.getWishesByListId($routeParams.list_id)
      .success(function(data) {
        $scope.formData = data;
      });
  }


  // CREATE ==================================================================
  $scope.createList = function() {
      if (!$.isEmptyObject($scope.formData)) {

          WishlistServer.createList($scope.formData)
              .success(function(data) {
                  $scope.formData = {};
                  $scope.items = data;
              });
      }
  };

  // UPDATE ==================================================================
  $scope.updateList = function() {
      if (!$.isEmptyObject($scope.formData)) {
          WishlistServer.putList($scope.formData._id, $scope.formData)
              .success(function() {
                  $scope.formData = {};
              });
      }
  };

  // DELETE ==================================================================
  $scope.deleteList = function(id) {
      WishlistServer.deleteList(id)
          .success(function(data) {
              $scope.items = data;
          });
  };
}]);
