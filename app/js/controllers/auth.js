'use strict';

angular.module('wishList')
.controller('AuthController', ['$scope', '$rootScope', '$localStorage',
'$routeParams', '$location', 'AuthService',
function($scope, $rootScope, $localStorage, $routeParams, $location, AuthService) {
  $scope.formData = {};
  $scope.signin = function() {
      AuthService.signin($scope.formData, function(res) {
          if (res.type == false) {
              alert(res.data)
          } else {
              $localStorage.token = res.data.token;
              $localStorage.user = res.data;
              window.location = "/";
          }
      }, function() {
          $rootScope.error = 'Failed to signin';
      })
  };

  $scope.signup = function() {
      AuthService.signup($scope.formData, function(res) {
          if (res.type == false) {
              alert(res.data)
          } else {
            AuthService.setupUser(res)
              window.location = "/"
          }
      }, function() {
          $rootScope.error = 'Failed to signup';
      })
  };

  $scope.me = function() {
      AuthService.me(function(res) {
          $scope.myDetails = res;
      }, function() {
          $rootScope.error = 'Failed to fetch details';
      })
  };

  $scope.logout = function() {
      AuthService.logout(function() {
          window.location = "/"
      }, function() {
          alert("Failed to logout!");
      });
  };
  // $scope.token = $localStorage.token;
}]);
