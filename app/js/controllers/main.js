'use strict';

angular.module('wishList').controller('MainController', ['$scope', '$routeParams', '$location', 'WishlistServer', '$localStorage', function($scope, $routeParams, $location, WishlistServer, $localStorage) {
  $scope.user = $localStorage.user ? $localStorage.user.login : '';
  $scope.greeting = 'Hola, ' + ($scope.user || 'guest') + '!';
}]);
