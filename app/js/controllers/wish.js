'use strict';

angular.module('wishList')
.controller('WishController', ['$scope', '$routeParams', '$location', 'WishlistServer', '$localStorage',
 function($scope, $routeParams, $location, WishlistServer, $localStorage) {

   //     img: 'http://www.peta.org/wp-content/uploads/2016/02/Goats-maximili-602x403.jpg',

   $scope.formData = {
     list: $routeParams.list_id
   };



   // GET =====================================================================
   $scope.getWishes = function () {
     if ($routeParams.wish_id) {
         WishlistServer.getWishById($routeParams.wish_id)
           .success(function(data) {
             $scope.formData = data;
             $scope.item = data;
           });
     } else if ($routeParams.list_id){
       WishlistServer.getWishesByListId($routeParams.list_id)
         .success(function(data) {
           $scope.items = data;
         });
     } else {
       WishlistServer.getWishes()
         .success(function(data) {
           $scope.items = data;
         });
     }
   }

   $scope.getWishes()


   // CREATE ==================================================================
   $scope.createWish = function() {
       if (!$.isEmptyObject($scope.formData)) {

           WishlistServer.createWish($scope.formData)
               .success(function(data) {
                   $scope.formData = {
                     list: $routeParams.list_id
                   };
                   $scope.items = data;
               });
       }
   };

   // UPDATE ==================================================================
   $scope.updateWish = function() {
       if (!$.isEmptyObject($scope.formData)) {
           WishlistServer.putWish($scope.formData._id, $scope.formData)
               .success(function() {
                   var path = $scope.formData.list ? '/lists/' + $scope.formData.list + '/wishes' : '/';
                   $scope.formData = {};
                   $location.path(path);
               });
       }
   };

   // DELETE ==================================================================
   $scope.deleteWish = function(wish_id, list_id) {
       WishlistServer.deleteWish(wish_id, list_id)
           .success(function(data) {
               $scope.getWishes()
           });
   };
}]);
