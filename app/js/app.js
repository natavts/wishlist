'use strict';

var app = angular.module('wishList',[
  'ngStorage',
  'ngRoute'
]);

app.config(['$locationProvider', '$routeProvider', '$httpProvider',
    function config($locationProvider, $routeProvider, $httpProvider) {
      $locationProvider.html5Mode(true);

      $routeProvider.
        when('/', {
          templateUrl: '/views/main.html',
          controller: 'WishController'
        }).
        when('/wishes/:wish_id/edit', {
          templateUrl: '/views/items/edit.html',
          controller: 'WishController'
        }).
        when('/lists', {
          templateUrl: '/views/lists.html',
          controller: 'ListController'
        }).
        when('/lists/:list_id/wishes', {
          templateUrl: '/views/wishes.html',
          controller: 'WishController'
        }).
        when('/profile', {
          templateUrl: '/views/profile.html',
          controller: 'ProfileController'
        }).
        // when('/lists/:list_id/wishes/:wish_id/edit', {
        //   templateUrl: '/views/items/edit.html',
        //   controller: 'MainController'
        // }).
        when('/signup', {
          templateUrl: '/views/signup.html',
          controller: 'AuthController'
        }).
        otherwise({redirectTo:'/'});


        $httpProvider.interceptors.push(['$q', '$location', '$localStorage', function($q, $location, $localStorage) {
            return {
                'request': function (config) {
                    config.headers = config.headers || {};
                    if ($localStorage.token) {
                        config.headers.Authorization = 'Bearer ' + $localStorage.token;
                    }
                    return config;
                },
                'responseError': function(response) {
                    if(response.status === 401 || response.status === 403) {
                        $location.path('/signup');
                    }
                    return $q.reject(response);
                }
            };
        }]);
    }
  ])

.factory('WishlistServer', function($http) {
    var baseUrl = 'http://192.168.2.12:8080';
    return {
        getLists : function() {
          return $http.get(baseUrl + '/api/lists');
        },
        createList : function(listData) {
          return $http.post(baseUrl + '/api/lists', listData);
        },
        deleteList : function(id) {
          return $http.delete(baseUrl + '/api/lists/' + id);
        },
        putList : function(id, listData) {
          return $http.put(baseUrl + '/api/lists/' + id, listData);
        },

        getWishes : function() {
            return $http.get(baseUrl + '/api/wishes');
        },
        getWishesByListId : function(list_id) {
            return $http.get(baseUrl + '/api/lists/'+ list_id +'/wishes');
        },
        createWish : function(wishData) {
            return $http.post(baseUrl + '/api/wishes', wishData);
        },
        deleteWish : function(wish_id, list_id) {
          return $http.delete(baseUrl + '/api/wishes/' + wish_id, {list_id: list_id});
        },
        putWish : function(id, wishData) {
            return $http.put(baseUrl + '/api/wishes/' + id, wishData);
        },
        getWishById : function(id) {
            return $http.get(baseUrl + '/api/wishes/' + id);
        }
    }
})

.factory('AuthService', ['$http', '$localStorage', function($http, $localStorage){
        var baseUrl = "http://192.168.2.12:8080";
        function changeUser(user) {
            angular.extend(currentUser, user);
        }

        function urlBase64Decode(str) {
            var output = str.replace('-', '+').replace('_', '/');
            switch (output.length % 4) {
                case 0:
                    break;
                case 2:
                    output += '==';
                    break;
                case 3:
                    output += '=';
                    break;
                default:
                    throw 'Illegal base64url string!';
            }
            return window.atob(output);
        }

        function getUserFromToken() {
            var token = $localStorage.token;
            var user = {};
            if (typeof token !== 'undefined') {
                var encoded = token.split('.')[1];
                user = JSON.parse(urlBase64Decode(encoded));
            }
            return user;
        }

        var currentUser = getUserFromToken();

        return {
            signup: function(data, success, error) {
                $http.post(baseUrl + '/signup', data).success(success).error(error)
            },
            signin: function(data, success, error) {
                $http.post(baseUrl + '/authenticate', data).success(success).error(error)
            },
            me: function(success, error) {
                $http.get(baseUrl + '/me').success(success).error(error)
            },
            logout: function(success) {
                changeUser({});
                delete $localStorage.token;
                delete $localStorage.user;
                success();
            },
            editUser: function(data, success, error) {
              $http.put(baseUrl + '/users', data).success(success).error(error)
            },
            setupUser: function(res) {
                $localStorage.token = res.data.token;
                $localStorage.user = res.data;
            }
        };
    }
]);
