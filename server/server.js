var express  = require('express');
var app      = express();
var mongoose = require('mongoose');
var morgan = require('morgan');             // log requests to the console (express4)
var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)

var config = require('./config');

var jwt        = require("jsonwebtoken");
var User     = require('./models/User');
var List     = require('./models/List');
var Wish     = require('./models/Wish');

// configuration =================

mongoose.connect(config.mongoUrl);     // connect to mongoDB database on modulus.io

// app.use(express.static(__dirname + '/public'));                 // set the static files location /public/img will be /img for users
app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());

process.on('uncaughtException', function(err) {
    console.log(err);
});

// listen (start app with node server.js) ======================================
app.listen(8080);
console.log("App listening on port 8080");

app.all("*", function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    return next();
});

// Routing ====================================================================

// authenticate ==========================
app.post('/authenticate', function(req, res) {
    User.findOne({email: req.body.email, password: req.body.password}, function(err, user) {
        if (err) {
            res.json({
                type: false,
                data: "Error occured: " + err
            });
        } else {
            if (user) {
               res.json({
                    type: true,
                    data: user,
                    token: user.token
                });
            } else {
                res.json({
                    type: false,
                    data: "Incorrect email/password"
                });
            }
        }
    });
});

// sign up ========================
app.post('/signup', function(req, res) {
    User.findOne({email: req.body.email, password: req.body.password}, function(err, user) {
        if (err) {
            res.json({
                type: false,
                data: "Error occured: " + err
            });
        } else {
            if (user) {
                res.json({
                    type: false,
                    data: "User already exists!"
                });
            } else {
                var userModel = new User();
                userModel.email = req.body.email;
                userModel.password = req.body.password;
                userModel.login = req.body.login;
                userModel.save(function(err, user) {
                    user.token = jwt.sign(user, config.secret); 
                    user.save(function(err, user1) {
                        res.json({
                            type: true,
                            data: user1,
                            token: user1.token
                        });
                    });
                })
            }
        }
    });
});

app.get('/me', ensureAuthorized, function(req, res) {
    User.findOne({token: req.token}, function(err, user) {
        if (err) {
            res.json({
                type: false,
                data: "Error occured: " + err
            });
        } else {
            res.json({
                type: true,
                data: user
            });
        }
    });
});

app.put('/users', ensureAuthorized, function(req, res) {
    User.findOne({token: req.token}, function(err, userModel) {
        if (err) {
            res.json({
                type: false,
                data: "Error occured: " + err
            });
        } else {

          userModel.email = req.body.email;
          userModel.password = req.body.password;
          userModel.login = req.body.login;
          userModel.save(function(err, user) {
              user.token = jwt.sign(user, config.secret);
              user.save(function(err, user1) {
                  res.json({
                      type: true,
                      data: user1,
                      token: user1.token
                  });
              });
          })
        }
    });
});

function ensureAuthorized(req, res, next) {
    var bearerToken;
    var bearerHeader = req.headers["authorization"];
    if (typeof bearerHeader !== 'undefined') {
        var bearer = bearerHeader.split(" ");
        bearerToken = bearer[1];
        req.token = bearerToken;
        req.user = jwt.verify(bearerToken, 'secret')._doc;
        next();
    } else {
        res.send(403);
    }
}

// Lists ===================
app.get('/api/lists', ensureAuthorized, function(req, res) {
    List.find({owner: req.user._id}, function(err, lists) {
        if (err) res.send(err)
        res.json(lists);
    });
});

app.post('/api/lists', ensureAuthorized, function(req, res) {
    List.create({
        name : req.body.name,
        owner: req.user._id
    }, function(err, list) {
        if (err) res.send(err);

        List.findByUser(req.user._id, function(err, lists) {
            if (err) res.send(err);
            res.json(lists);
        });
    });

});

app.delete('/api/lists/:list_id', ensureAuthorized, function(req, res) {
    List.remove({
        _id : req.params.list_id,
        owner: req.user._id
    }, function(err) {
        if (err) res.send(err);

        Wish.remove({
            list : req.params.list_id
        }, function(err, wish) {
            if (err) res.send(err);
        });

        List.findByUser(req.user._id, function(err, lists) {
            if (err) res.send(err);
            res.json(lists);
        });
    });
});

app.put('/api/lists/:list_id', ensureAuthorized, function(req, res) {
  List.findOne({_id : req.params.list_id, owner: req.user._id}, function(err, list) {

    if (err) res.send(err);

    list.name = req.body.name;

    list.save(function(err) {
      if (err)
        res.send(err);

      res.json({ message: 'List updated!' });
    });
  });
});

// Wishes ===================
app.get('/api/wishes', function(req, res) {

    Wish.find(function(err, wishes) {
        if (err) res.send(err);

        res.json(wishes);
    });
});

app.get('/api/wishes/:wish_id', ensureAuthorized, function(req, res) {

    Wish.findOne({_id : req.params.wish_id, owner: req.user._id}, function(err, wish) {
        if (err) res.send(err);

        res.json(wish);
    });
});

app.get('/api/lists/:list_id/wishes', ensureAuthorized, function(req, res) {

  Wish.find({list: req.params.list_id, owner: req.user._id}, function(err, wishes) {
      if (err) res.send(err);

      res.json(wishes);
  });
});

app.post('/api/wishes', ensureAuthorized, function(req, res) {
    Wish.create({
        name : req.body.name,
        description : req.body.description,
        img: req.body.img,
        list: req.body.list,
        owner: req.user._id
    }, function(err, wish) {
        if (err) res.send(err);

        Wish.findByList(req.user._id, req.body.list, function(err, wishes) {
            if (err) res.send(err)
            res.json(wishes);
        });
    });

});

app.delete('/api/wishes/:wish_id', ensureAuthorized, function(req, res) {
    Wish.remove({
        _id : req.params.wish_id,
        owner: req.user._id
    }, function(err, wish) {
        if (err) res.send(err);

        res.json('wish deleted');
    });
});

app.put('/api/wishes/:wish_id', ensureAuthorized, function(req, res) {
  Wish.findOne({_id: req.params.wish_id, owner: req.user._id}, function(err, wish) {
    if (err) res.send(err);

    wish.name = req.body.name;
    wish.description = req.body.description;
    wish.img = req.body.img;
    wish.save(function(err) {
      if (err) res.send(err);

      res.json({ message: 'Wish updated!' });
    });

  });
});
