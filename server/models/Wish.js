var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var ObjectId = mongoose.Schema.ObjectId;

var WishSchema   = new Schema({
  name : String,
  description : String,
  img: String,
  list: ObjectId,
  owner: ObjectId
});

WishSchema.statics.findByList = function(user_id, list_id, cb) {
  return this.find({ list: list_id, owner: user_id }, cb);
};

module.exports = mongoose.model('Wish', WishSchema);
