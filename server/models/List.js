var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var ObjectId = mongoose.Schema.ObjectId;

var ListSchema   = new Schema({
  name : String,
  owner: ObjectId
});

ListSchema.statics.findByUser = function(user_id, cb) {
  return this.find({ owner: user_id }, cb);
};

module.exports = mongoose.model('List', ListSchema);
